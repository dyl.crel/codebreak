﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Codebreak.Framework.Generic;
using Codebreak.Service.World.Database.Repository;
using Codebreak.Service.World.Database.Structure;
using Codebreak.Service.World.Game.Action;
using Codebreak.Service.World.Game.Entity;
using Codebreak.Service.World.Game.Job;
using Codebreak.Service.World.Game.Map;
using Codebreak.Service.World.Network;

namespace Codebreak.Service.World.Game.Interactive.Type
{
    public sealed class Fountain : InteractiveObject
    {
        /// <summary>
        /// 
        /// </summary>
        public const int FRAME_FARMING = 3;

        /// <summary>
        /// 
        /// </summary>
        public const int FRAME_CUT = 4;
        public int respawnTime { get; set; }
        private ItemTemplateDAO m_generatedTemplate;
        private UpdatableTimer m_fountainTimer;
        private CharacterEntity m_character;
        private int quantity;
        public int GeneratedTemplateId
        {
            get;
            private set;
        }

        public Fountain(MapInstance map, int cellId, int respawntime, bool canWalkThrough = false) : base(map, cellId, canWalkThrough)
        {
            respawnTime = respawnTime;
        }

        public ItemTemplateDAO GeneratedTemplate
        {
            get
            {
                if (m_generatedTemplate == null)
                    m_generatedTemplate = ItemTemplateRepository.Instance.GetById(311);
                return m_generatedTemplate;
            }
        }

        public override void UseWithSkill(CharacterEntity character, JobSkill skill)
        {
            switch (skill.Id)
            {
                case SkillIdEnum.SKILL_PUISER:
                    FountainUse(character, skill.Id);
                    break;
            }
        }

        private void FountainUse(CharacterEntity character, SkillIdEnum skill)
        {
            if(!character.CanGameAction(Game.Action.GameActionTypeEnum.SKILL_USE))
            {
                return;
            }

            quantity = Util.Next(1, 5);


            character.FountainStart(this, 12000);

            m_character = character;

            Deactivate();

            m_fountainTimer = base.AddTimer(12000, StopFountain, true);
        }

        private void StopFountain()
        {
            m_character.StopAction(GameActionTypeEnum.SKILL_USE);


            m_character.CachedBuffer = true;
            m_character.Inventory.AddItem(GeneratedTemplate.Create(quantity));
            m_character.Dispatch(WorldMessage.INTERACTIVE_FARMED_QUANTITY(m_character.Id, quantity));
            m_character.CachedBuffer = false;

            base.UpdateFrame(FRAME_FARMING, FRAME_CUT);

            base.AddTimer(Util.Next(12000, 12000), Respawn, true);
        }

        private void Respawn()
        {
            base.UpdateFrame(FRAME_CUT, FRAME_NORMAL, true);
        }
    }
}
