﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Codebreak.Service.World.Database.Repository;
using Codebreak.Service.World.Database.Structure;
using Codebreak.Service.World.Game.Entity;
using Codebreak.Service.World.Game.Stats;
using Codebreak.Service.World.Network;

namespace Codebreak.Service.World.Game.ActionEffect
{
    public class AddQuestEffect : AbstractActionEffect<AddQuestEffect>
    {
        public override bool Process(CharacterEntity character, Dictionary<string, string> parameters)
        {
            throw new NotImplementedException();
        }

        public override bool Process(CharacterEntity character)
        {
            throw new NotImplementedException();
        }

        public override bool Process(CharacterEntity character, string parameters)
        {
            var questid = int.Parse(parameters);
            QuestDAO Quest = QuestRepository.Instance.Find(c => c.Id == questid);
            CharacterQuestDAO CharacterQuest = new CharacterQuestDAO()
            {
                Id = (int)character.Id,
                CurrentStepId = Quest.Steps.Select(c => c.Id).FirstOrDefault(),
                Done = false,
                SerializedObjectives = Quest.Objectives.ToString() + ":1",
                QuestId = int.Parse(parameters)
            };
            if (CharacterQuestRepository.Instance.Insert(CharacterQuest) == true)
            {
                character.Dispatch(WorldMessage.INFORMATION_MESSAGE(InformationTypeEnum.INFO, InformationEnum.INFOS_QUEST_NEW, Quest.Id));
                character.Quests.Add(new Game.Quest.CharacterQuest(character, CharacterQuest));
                return true;
            }
            else
                return false;
        }

        public override bool ProcessItem(CharacterEntity character, ItemDAO item, GenericEffect effect, long targetId, int targetCell)
        {
            throw new NotImplementedException();
        }
    }
}
