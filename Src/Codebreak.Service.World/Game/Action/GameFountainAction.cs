﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Codebreak.Service.World.Game.Entity;
using Codebreak.Service.World.Game.Interactive.Type;

namespace Codebreak.Service.World.Game.Action
{
    public class GameFountainAction : AbstractGameAction
    {

        public override bool CanAbort => false;

        public Fountain m_fountain
        {
            get;
        }

        public GameFountainAction(CharacterEntity character, Fountain fountain, long duration) : base(GameActionTypeEnum.SKILL_USE, character, duration)
        {
            m_fountain = fountain;
        }

        public override string SerializeAs_GameAction()
        {
            return m_fountain.CellId + "," + Duration;
        }
    }
}
