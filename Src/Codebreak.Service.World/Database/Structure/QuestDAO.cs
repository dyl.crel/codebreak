﻿using Codebreak.Framework.Database;
using Codebreak.Service.World.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codebreak.Service.World.Database.Structure
{
    [Table("quest_data")]
    public sealed class QuestDAO : DataAccessObject<QuestDAO>
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Etapes { get; set; }
        public string Objectives { get; set; }
        public int Npc { get; set; }
        public string Condition { get; set; }
        public string RequiredLevel { get; set; }

        [Write(false)]
        public List<QuestStepDAO> result { get; set; } = new List<QuestStepDAO>();
        [Write(false)]
        public List<QuestStepDAO> Steps
        {
            get
            {
                foreach (var item in Etapes.Split(';'))
                {
                    if (!string.IsNullOrWhiteSpace(item) && QuestStepRepository.Instance.Find(c => c.Id == int.Parse(item)) != null)
                        result.Add(QuestStepRepository.Instance.Find(c => c.Id == int.Parse(item)));
                }
                return result;
            }
        }
        [Write(false)]
        public List<QuestObjectiveDAO> ResultObjectives { get; set; } = new List<QuestObjectiveDAO>();
        [Write(false)]
        public List<QuestObjectiveDAO> ListObjectives
        {
            get
            {
                foreach (var item in Objectives.Split(';'))
                {
                    if (!string.IsNullOrWhiteSpace(item) && QuestObjectiveRepository.Instance.Find(c => c.Id == int.Parse(item)) != null)
                        ResultObjectives.Add(QuestObjectiveRepository.Instance.Find(c => c.Id == int.Parse(item)));
                }
                return ResultObjectives;
            }
        }
    }
}
