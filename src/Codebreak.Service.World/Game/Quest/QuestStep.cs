﻿using Codebreak.Service.World.Database.Structure;
using Codebreak.Service.World.Game.ActionEffect;
using Codebreak.Service.World.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codebreak.Service.World.Game.Quest
{
    public sealed class QuestStep
    {
        public int Id => m_record.Id;
        public string Name => m_record.Name;
        public string Description => m_record.Description;
        public int Xp => m_record.Xp;
        public int Kamas => m_record.Kamas;
        public string Object => m_record.Object;
        public ActionList ActionsList => m_record.ActionsList;

        public List<AbstractQuestObjective> Objectives { get; }

        private readonly QuestStepDAO m_record;
        public QuestStep(QuestStepDAO record, QuestDAO m_quest)
        {
            m_record = record;
            Objectives = m_quest.ListObjectives.Select(AbstractQuestObjective.FromRecord).ToList();

            QuestManager.Instance.AddStep(this);
        }
    }
}
