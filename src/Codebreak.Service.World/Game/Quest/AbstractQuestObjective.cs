﻿using Codebreak.Service.World.Database.Structure;
using Codebreak.Service.World.Game.Quest.Impl;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codebreak.Service.World.Game.Quest
{
    public enum QuestObjectiveType
    {
        GENERIC = 0,
        FIND_NPC = 1,
        ITEM_TO = 2,
        BRING_BACK_TO = 3,
        DISCOVER_THE_MAP = 4,
        DISCOVER_THE_ZONE = 5,
        KILL_MONSTER = 6,
        USE = 8,
        GOING_BACK_TO = 9,
        ESCORT_NPC = 10,
        KILL_PLAYER = 11,
        REPORT_AME = 12,
        KILL_THEM = 13
    }

    public abstract class AbstractQuestObjective
    {
        protected static ILog Logger = LogManager.GetLogger(typeof(AbstractQuestObjective));

        public int Id => m_record.Id;
        public int Type => m_record.Type;
        public string Objective => m_record.Objective;
        public string Object => m_record.Object;
        public int Npc => m_record.Npc;
        public string Monster => m_record.Monster;
        public string Conditions => m_record.Monster;

        protected QuestObjectiveDAO m_record;

        protected AbstractQuestObjective(QuestObjectiveDAO record)
        {
            m_record = record;
        }

        public abstract bool Done(string value);

        public static AbstractQuestObjective FromRecord(QuestObjectiveDAO record)
        {
            switch ((QuestObjectiveType)record.Type)
            {
                case QuestObjectiveType.GENERIC: return new GenericObjective(record);
                case QuestObjectiveType.FIND_NPC: return new FindNpcObjective(record);
                case QuestObjectiveType.ITEM_TO: return new GenericObjective(record);
                case QuestObjectiveType.BRING_BACK_TO: return new GenericObjective(record);
                case QuestObjectiveType.DISCOVER_THE_MAP: return new GenericObjective(record);
                case QuestObjectiveType.DISCOVER_THE_ZONE: return new GenericObjective(record);
                case QuestObjectiveType.KILL_MONSTER: return new KillMonsterObjective(record);
                case QuestObjectiveType.USE: return new GenericObjective(record);
                case QuestObjectiveType.GOING_BACK_TO: return new GenericObjective(record);
                case QuestObjectiveType.ESCORT_NPC: return new GenericObjective(record);
                case QuestObjectiveType.KILL_PLAYER: return new GenericObjective(record);
                case QuestObjectiveType.REPORT_AME: return new GenericObjective(record);
                case QuestObjectiveType.KILL_THEM: return new GenericObjective(record);
            }
            throw new Exception("AbstractQuestObjective::FromType unknow typeId=" + record.Type);
        }
    }
}
