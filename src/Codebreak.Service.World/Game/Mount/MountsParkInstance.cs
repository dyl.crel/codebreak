﻿using Codebreak.Service.World.Database.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codebreak.Service.World.Game.Mount
{
    public sealed class MountsParkInstance
    {
        public int map => m_record.Map;
        public int owner => m_record.Owner;
        public int guild => m_record.Guild;
        public int price => m_record.Price;
        public string data => m_record.Data;
        public string enclos => m_record.Enclos;
        public string objectplacer => m_record.ObjectPlacer;
        public string durabilite => m_record.Durabilite;

        private readonly MountsParkInstanceDAO m_record;

        public MountsParkInstance(MountsParkInstanceDAO record)
        {
            m_record = record;
        }
    }
}
