﻿using Codebreak.Service.World.Database.Structure;
using Codebreak.Service.World.Game.Condition;
using Codebreak.Service.World.Game.Entity;
using Codebreak.Service.World.Manager;
using Codebreak.Service.World.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codebreak.Service.World.Game.Dialog
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class NpcDialog
    {
        /// <summary>
        /// 
        /// </summary>
        public const string BANK_COST = "%bankCost%";
        public const string NAME = "[name]";

        /// <summary>
        /// 
        /// </summary>
        private CharacterEntity Character
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        private NonPlayerCharacterEntity Npc
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public NpcQuestionDAO CurrentQuestion
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        private IEnumerable<NpcResponseDAO> m_possibleResponses;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="character"></param>
        /// <param name="npc"></param>
        public NpcDialog(CharacterEntity character, NonPlayerCharacterEntity npc)
        {
            Character = character;
            Npc = npc;
        }
        public NpcQuestionDAO RecursiveCall(NpcQuestionDAO question)
        {
            CurrentQuestion = question;
            if (!ConditionParser.Instance.Check(question.Conditions, Character))
                return RecursiveCall(NpcQuestionDAO.GetQuestion(int.Parse(CurrentQuestion.IfFalse)));
            else


                return CurrentQuestion;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="question"></param>
        public void SendQuestion(NpcQuestionDAO question)
        {

            CurrentQuestion = RecursiveCall(question);
            m_possibleResponses = CurrentQuestion.ResponseList;
            if (!string.IsNullOrWhiteSpace(Npc.Quest))
            {
                if (m_possibleResponses.Count() > 0)
                {
                    if (CharacterQuestDAO.HaveQuest(int.Parse(Npc.Quest), (int)Character.Id))
                    {
                        Character.Dispatch(WorldMessage.DIALOG_QUESTION(CurrentQuestion.Id, ApplyParameter(), m_possibleResponses.Select(response => response.Id)));
                    }
                    else
                    {
                        ActionEffectManager.Instance.ApplyEffect(Character, Spell.EffectEnum.BddStartQuest, Npc.Quest);
                        Character.Dispatch(WorldMessage.DIALOG_QUESTION(CurrentQuestion.Id, ApplyParameter(), m_possibleResponses.Select(response => response.Id)));
                    }
                }
                else
                {
                    Character.Dispatch(WorldMessage.DIALOG_QUESTION(CurrentQuestion.Id, ApplyParameter()));
                }
            }
            else
            {
                if (m_possibleResponses.Count() > 0)
                {
                   
                        Character.Dispatch(WorldMessage.DIALOG_QUESTION(CurrentQuestion.Id, ApplyParameter(), m_possibleResponses.Select(response => response.Id)));
                }
                else
                {
                    Character.Dispatch(WorldMessage.DIALOG_QUESTION(CurrentQuestion.Id, ApplyParameter()));
                }
            }


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="responseId"></param>
        public void ProcessResponse(int responseId)
        {
            var response = m_possibleResponses
                .First(entry => entry.Id == responseId);
            //if(response == null || !ConditionParser.Instance.Check(response.Conditions, Character))
            if (response == null)
            {
                Character.Dispatch(WorldMessage.BASIC_NO_OPERATION());
                return;
            }
            if (!string.IsNullOrWhiteSpace(response.Actions))
                ActionEffectManager.Instance.ApplyEffect(Character, response.Type, response.Actions);
            else
                ActionEffectManager.Instance.ApplyEffect(Character, response.Type);
        }

        /// <summary>
        /// 
        /// </summary>
        private string ApplyParameter()
        {
            switch (CurrentQuestion.Params)
            {
                case BANK_COST:
                    return Character.Bank.Items.GroupBy(item => item.TemplateId).Count().ToString();

                case NAME:
                    return Character.Name;
            }
            return "";
        }
    }
}
