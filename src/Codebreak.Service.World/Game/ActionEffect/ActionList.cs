﻿using Codebreak.Service.World.Game.Spell;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codebreak.Service.World.Game.ActionEffect
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class ActionEntry
    {
        public EffectEnum Effect { get; }
        public Dictionary<string, string> Parameters { get; }
        public ActionEntry(EffectEnum effect, Dictionary<string, string> parameters)
        {
            Effect = effect;
            Parameters = parameters;
        }
        public static ActionEntry Deserialize(string data)
        {
            var splitted = data.Split(':');
            var effect = (EffectEnum)int.Parse(splitted[0]);
            var parameters = new Dictionary<string, string>();
            foreach (var parameter in splitted[1].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                var splittedParam = parameter.Split('=');
                var key = splittedParam[0];
                var value = splittedParam[1];
                parameters.Add(key, value);
            }
            return new ActionEntry(effect, parameters);
        }
        public static ActionEntry Deserialize(string data, string ActionId)
        {
            var splitted = data.Split(',');
            var effect = (EffectEnum)int.Parse(ActionId);
            var parameters = new Dictionary<string, string>();
            parameters.Add(splitted[0], splitted[1]);
            return new ActionEntry(effect, parameters);
        }
        public static ActionEntry DeserializeForStep(string data, int i)
        {
            var splitted = data.Split(';');
            var parameters = new Dictionary<string, string>();
            EffectEnum effect;
            switch (i)
            {
                case 0:
                    effect = EffectEnum.AddExperience;
                    parameters.Add("experience", splitted[0]);
                    return new ActionEntry(effect, parameters);
                case 1:
                    effect = EffectEnum.AddKamas;
                    parameters.Add("kamas", splitted[0]);
                    return new ActionEntry(effect, parameters);
                case 2:
                    effect = EffectEnum.BddAddItem;
                    for (int x = 0; x < splitted.Length; x++)
                    {
                        var temp = splitted[x].Split(',');
                        if (int.Parse(temp[1]) > 1)
                            for (int f = 1; f < int.Parse(temp[1]); f++)
                            {
                                parameters.Add("itemId", temp[0]);
                            }
                        else
                            parameters.Add("itemId", temp[0]);
                    }
                    return new ActionEntry(effect, parameters);
                default:
                    break;
            }
            return null;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public sealed class ActionList : List<ActionEntry>
    {
        private static ILog Logger = LogManager.GetLogger(typeof(ActionList));

        public static ActionList Deserialize(string data, string ActionId)
        {
            var list = new ActionList();
            try
            {
                list.Add(ActionEntry.Deserialize(data, ActionId));
            }
            catch (Exception e)
            {
                Logger.Error("ActionList::Deserialize failed, check the script syntax, data=" + data, e);
            }
            return list;
        }

        public static ActionList Deserialize(string data)
        {
            var list = new ActionList();
            try
            {
                list.AddRange(data.Split('|').Select(ActionEntry.Deserialize));
            }
            catch (Exception e)
            {
                Logger.Error("ActionList::Deserialize failed, check the script syntax, data=" + data, e);
            }
            return list;
        }
        public static ActionList DeserializeForStep(int xp, int kamas, string objet)
        {
            var list = new ActionList();
            try
            {
                string[] TabVar = new string[] { xp.ToString(), kamas.ToString(), objet };
                for (int i = 0; i < TabVar.Length; i++)
                {

                    list.Add(ActionEntry.DeserializeForStep(TabVar[i], i));
                }
            }
            catch (Exception e)
            {

                Logger.Error("ActionList::Deserialize failed, check the script syntax, data=", e);
            }
            return list;
        }
    }
}
