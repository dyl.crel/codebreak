﻿using Codebreak.Framework.Database;
using Codebreak.Service.World.Database.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codebreak.Service.World.Database.Repository
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class NpcResponseRepository : Repository<NpcResponseRepository, NpcResponseDAO>
    {
        private List<KeyValuePair<int, NpcResponseDAO>> m_reponseById;

        /// <summary>
        /// 
        /// </summary>
        public NpcResponseRepository()
        {
            m_reponseById = new List<KeyValuePair<int, NpcResponseDAO>>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        public override void OnObjectAdded(NpcResponseDAO response)
        {
            m_reponseById.Add(new KeyValuePair<int,NpcResponseDAO>(response.Id, response));

            base.OnObjectAdded(response);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="question"></param>
        public override void OnObjectRemoved(NpcResponseDAO response)
        {
            m_reponseById.Remove(new KeyValuePair<int,NpcResponseDAO>(response.Id,response));

            base.OnObjectRemoved(response);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reponseId"></param>
        /// <returns></returns>
        public NpcResponseDAO GetById(int reponseId)
        {
            return m_reponseById.Where(c => c.Key == reponseId).Select(c => c.Value).FirstOrDefault();
        }

        public override void UpdateAll(MySql.Data.MySqlClient.MySqlConnection connection, MySql.Data.MySqlClient.MySqlTransaction transaction)
        {
        }

        public override void DeleteAll(MySql.Data.MySqlClient.MySqlConnection connection, MySql.Data.MySqlClient.MySqlTransaction transaction)
        {
        }

        public override void InsertAll(MySql.Data.MySqlClient.MySqlConnection connection, MySql.Data.MySqlClient.MySqlTransaction transaction)
        {
        }
    }
}
