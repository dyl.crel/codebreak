﻿using Codebreak.Framework.Database;
using Codebreak.Service.World.Database.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codebreak.Service.World.Database.Repository
{
    public sealed class MountsParkInstanceRepository : Repository<MountsParkInstanceRepository, MountsParkInstanceDAO>
    {
        public override void Initialize(SqlManager sqlMgr)
        {
            base.Initialize(sqlMgr);
        }
    }
}
