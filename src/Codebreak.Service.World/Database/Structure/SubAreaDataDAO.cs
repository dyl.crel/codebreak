﻿using Codebreak.Framework.Database;
using Codebreak.Service.World.Database.Repository;
using System.Collections.Generic;

namespace Codebreak.Service.World.Database.Structure
{
    /// <summary>
    /// 
    /// </summary>
    [Table("subarea_data")]
    public sealed class SubAreaDataDAO : DataAccessObject<SubAreaDataDAO>
    {
        [Key]
        public int Id
        {
            get;
            set;
        }

        public int AreaId
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }
    }
}
