﻿using Codebreak.Framework.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codebreak.Service.World.Database.Structure
{
    [Table("quest_data")]
    public sealed class QuestDataDAO : DataAccessObject<QuestDataDAO>
    {
        [Key]
        public int Id { get; set; }
        public string Description { get; set; }
        public string Steps { get; set; }
        public string Objectives { get; set; }
        public int Npc { get; set; }
        public string Condition { get; set; }
        public string RequiredLevel { get; set; }
    }
}
