﻿using Codebreak.Framework.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codebreak.Service.World.Database.Structure
{
    [Table("quest_objectives")]
    public sealed class QuestObjectivesDAO : DataAccessObject<QuestObjectivesDAO>
    {
        [Key]
        public int Id { get; set; }
        public int Type { get; set; }
        public string Objective { get; set; }
        public string Object { get; set; }
        public int Npc { get; set; }
        public string Monster { get; set; }
        public string Conditions { get; set; }
    }
}
