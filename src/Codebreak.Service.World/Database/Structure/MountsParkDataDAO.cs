﻿using Codebreak.Framework.Database;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codebreak.Service.World.Database.Structure
{    
    [Table("mounts_park_data")]
    [ImplementPropertyChanged]
    public sealed class MountsParkDataDAO : DataAccessObject<MountsParkDataDAO>
    {
        [Key]
        public int MapId { get; set; }
        public long DefaultPrice { get; set; }
        public int PlaceMount { get; set; }
        public int PlaceObject { get; set; }
        public int CellId { get; set; }
        public int CellMount { get; set; }
        public int CellPorte { get; set; }
        public string CellEnclos { get; set; }
    }
}
