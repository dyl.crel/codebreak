﻿using Codebreak.Framework.Database;
using Codebreak.Service.World.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codebreak.Service.World.Database.Structure
{
    [Table("characterquest")]
    public sealed class CharacterQuestDAO : DataAccessObject<CharacterQuestDAO>
    {
        [Key]
        public int Id { get; set; }
        public bool Done { get; set; }
        public int CurrentStepId { get; set; }
        public string SerializedObjectives { get; set; }
        public int QuestId { get; set; }

        public static bool HaveQuest(int id, int characterid)
        {
            if (CharacterQuestRepository.Instance.Find(c => c.QuestId == id && characterid == c.Id) != null)
                return true;
            else
                return false;
        }
        public static bool HaveFinishQuest(int id, int characterid)
        {
            if (CharacterQuestRepository.Instance.Find(c => c.QuestId == id && characterid == c.Id && c.Done == true) != null)
                return true;
            else
                return false;
        }
    }
}
