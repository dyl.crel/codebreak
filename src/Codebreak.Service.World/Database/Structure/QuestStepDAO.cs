﻿using Codebreak.Framework.Database;
using Codebreak.Service.World.Game.ActionEffect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codebreak.Service.World.Database.Structure
{
    [Table("quest_steps")]
    public sealed class QuestStepDAO : DataAccessObject<QuestStepDAO>
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Xp { get; set; }
        public int Kamas { get; set; }
        public string Object { get; set; }

        /// <summary>
        /// 
        /// </summary>
        private ActionList m_actions;

        /// <summary>
        /// 
        /// </summary>
        [Write(false)]
        public ActionList ActionsList
        {
            get
            {
                if (m_actions == null)                
                    m_actions = ActionList.DeserializeForStep(Xp,Kamas,Object);                
                return m_actions;
            }
        }
        
    }
}
