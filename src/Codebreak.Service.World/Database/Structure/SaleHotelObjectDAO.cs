﻿using Codebreak.Framework.Database;
using Codebreak.Service.World.Database.Repository;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codebreak.Service.World.Database.Structure
{
    /// <summary>
    /// 
    /// </summary>
    [Table("sale_hotel_object")]
    [ImplementPropertyChanged]
    public sealed class SaleHotelObjectDAO : DataAccessObject<SaleHotelObjectDAO>
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public long IdObject
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int IdHotel
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public long Owner
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public long Price
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime ExpireDate
        {
            get;
            set;
        }        
    }
}
