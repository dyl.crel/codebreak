﻿using Codebreak.Framework.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codebreak.Service.World.Database.Structure
{
    /// <summary>
    /// 
    /// </summary>
    [Table("monsters_race")]
    public sealed class MonstersRaceDAO : DataAccessObject<MonstersRaceDAO>
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public int Id
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get;
            set;
        }
        /// <summary>
        /// 
        /// </summary>
        public int SuperRaceId
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        private List<MonstersDAO> m_monsters = new List<MonstersDAO>();

        /// <summary>
        /// 
        /// </summary>
        [Write(false)]
        public List<MonstersDAO> Monsters => m_monsters;
    }
}
