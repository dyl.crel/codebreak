﻿using Codebreak.Framework.Database;
using Codebreak.Service.World.Database.Repository;
using System.Collections.Generic;

namespace Codebreak.Service.World.Database.Structure
{
    [Table("superarea_data")]
    public sealed class SuperAreaDataDAO : DataAccessObject<SuperAreaDataDAO>
    {
        [Key]
        public int Id
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }
    }
}
