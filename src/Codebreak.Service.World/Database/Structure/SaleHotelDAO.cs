﻿using Codebreak.Framework.Database;
using Codebreak.Service.World.Database.Repository;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codebreak.Service.World.Database.Structure
{
    /// <summary>
    /// 
    /// </summary>
    [Table("sale_hotel")]
    public sealed class SaleHotelDAO : DataAccessObject<SaleHotelDAO>
    {
        private int result;
        [Key]
        public int Id
        {
            get;
            set;
        }
        public int MapId
        {
            get;
            set;
        }
        public IEnumerable<int> ListCategory
        {
            get
            {
                return Category.Split(',').Select(int.Parse).ToList();
            }
            set { }
        }
        public string Category
        {
            get;
            set;
        }
        public int NpcId
        {
            get
            {
                foreach (var item in NpcInstanceRepository.Instance.SelectAll.Where(c => c.MapId == MapId))
                {
                    result = item.TemplateId;
                    break;
                }
                return result;
            }
            set { }
        }

        public int ItemMaxLevel
        {
            get;
            set;
        }

        public int AccountItem
        {
            get;
            set;
        }

        public long Timeout
        {
            get;
            set;
        }

        public int SellTaxe
        {
            get;
            set;
        }
        public int GetNpcId(int mapId)
        {
            return NpcInstanceRepository.Instance.SelectAll.Where(c => c.MapId == MapId).Select(c => c.Id).FirstOrDefault();
        }
    }
}
