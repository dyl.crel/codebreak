﻿using Codebreak.Framework.Database;
using Codebreak.Service.World.Database.Repository;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codebreak.Service.World.Database.Structure
{
    /// <summary>
    /// 
    /// </summary>
    [Table("guild_members")]
    [ImplementPropertyChanged]
    public sealed class GuildMemberDAO : DataAccessObject<GuildMemberDAO>
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public long Id
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public long GuildId
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int Rank
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int Rights
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public int XPPercentage
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public long XPGiven
        {
            get;
            set;
        }
        public static IEnumerable<GuildMemberDAO> GetMembers()
        {
            return CharacterGuildRepository.Instance.All;
        }
    }
}
