﻿using Codebreak.Framework.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codebreak.Service.World.Database.Structure
{
    [Table("quest_objectives_type")]
    public class QuestObjectivesTypeDAO : DataAccessObject<QuestObjectivesTypeDAO>
    {
        [Key]
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
