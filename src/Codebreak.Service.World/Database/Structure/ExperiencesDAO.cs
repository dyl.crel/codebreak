﻿using Codebreak.Framework.Database;

namespace Codebreak.Service.World.Database.Structure
{
    /// <summary>
    /// 
    /// </summary>
    [Table("experiences")]
    public sealed class ExperiencesDAO : DataAccessObject<ExperiencesDAO>
    {
        [Key]
        public int Level
        {
            get;
            set;
        }
        public long Character
        {
            get;
            set;
        }
        public long Job
        {
            get;
            set;
        }
        public long Mount
        {
            get;
            set;
        }
        public long Pvp
        {
            get;
            set;
        }
        public long Guild
        {
            get;
            set;
        }
    }
}
