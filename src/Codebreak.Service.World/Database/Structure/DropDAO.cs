﻿using Codebreak.Framework.Database;
using Codebreak.Service.World.Database.Repository;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codebreak.Service.World.Database.Structure
{
    /// <summary>
    /// 
    /// </summary>
    [Table("drops")]
    public sealed class DropDAO : DataAccessObject<DropDAO>
    {
        [Key]
        public int Id
        {
            get;
            set;
        }
        public int MonsterId
        {
            get;
            set;
        }
        public string MonsterName
        {
            get;
            set;
        }
        public int TemplateId
        {
            get;
            set;
        }
        public int PPThreshold
        {
            get;
            set;
        }
        public int Max
        {
            get;
            set;
        }
        public double Rate
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        private MonstersDAO m_monster;
        [Write(false)]
        [DoNotNotify]
        public MonstersDAO Monster
        {
            get
            {
                if (m_monster == null)
                    m_monster = MonsterRepository.Instance.GetById(MonsterId);
                return m_monster;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private ItemsDataDAO m_item;
        [Write(false)]
        [DoNotNotify]
        public ItemsDataDAO ItemTemplate
        {
            get
            {
                if (m_item == null)
                    m_item = ItemTemplateRepository.Instance.GetById(TemplateId);
                return m_item;
            }
        }
    }
}
