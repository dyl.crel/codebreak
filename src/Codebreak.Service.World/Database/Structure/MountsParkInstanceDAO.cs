﻿using Codebreak.Framework.Database;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codebreak.Service.World.Database.Structure
{
    [Table("mounts_park_instance")]
    public class MountsParkInstanceDAO : DataAccessObject<MountsParkInstanceDAO>
    {
        [Key]
        public int Map
        {
            get;
            set;
        }
        public int Owner
        {
            get;
            set;
        }
        public int Guild
        {
            get;
            set;
        }
        public int Price
        {
            get;
            set;
        }
        public string Data
        {
            get;
            set;
        }
        public string Enclos
        {
            get;
            set;
        }
        public string ObjectPlacer
        {
            get;
            set;
        }
        public string Durabilite
        {
            get;
            set;
        }
    }
}
